//
// Available resolutions
//
var resolutions = [
  {width: 240, height: 180},
  {width: 320, height: 240},
  {width: 640, height: 480},
  {width: 800, height: 600},
  {width: 1024, height: 768},
  {width: 1280, height: 960},
  {width: 1850, height: 1387},
];

// Add a btnId property to each element
resolutions.forEach((r) => {
  r.btnId = `webcam-start-${r.width}-${r.height}`;
});

//
// Starts Fullscreen mode if available
//
function start_fullscreen() {
  var video = document.getElementsByTagName('video')[0];
  if (video && video.requestFullscreen) {
    video.requestFullscreen();
  } else {
    $('#resolution-buttons').append( ``
    + `<div class="alert alert-dismissible alert-danger">`
    + `  <button type="button" class="close" data-dismiss="alert">×</button>`
    + `  ERROR: Fullscreen not allowed`
    + `</div>`
      );
  }
}

//
// Starts webcam @ specified resolution
//
function webcam_start(width, height) {
  // Animate preview size change
  $( "#camera-preview" ).animate({
    width: width,
    height: height,
  }, 500, function() {
    // Animation complete, reset cam
    Webcam.reset();
    Webcam.set({
      width: width,
      height: height,
      dest_width: 640,
      dest_height: 480,
      flip_horiz: false,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#camera-preview' );
  });

  // Radio button effect
  resolutions.forEach((r) => {
    $(`#${r.btnId}`).removeClass('active');
  });
  $(`#webcam-start-${width}-${height}`).addClass('active');
}

//
// Add a new snapshot to the snapshots panel
//
function show_snapshot(data_uri) {
  var html = ``
    + `<div class="snapshot-container shadow">`
    + `  <div class="shapshot-image">`
    + `    <a href="${data_uri}" target="_blank">`
    + `      <img class="snapshot" src="${data_uri}"/>`
    + `    </a>`
    + `  </div>`
    + `  <div class="snapshot-caption">`
    + `    <span class="label label-default lbl-timestamp">${moment().format('H:mm:ss')}</span>`
    + `    <span class="label label-default lbl-resolution">${Webcam.params.dest_width}x${Webcam.params.dest_height}</span>`
    + `  </div>`
    + `</div>`
    + ``;

  $('#snapshots').append( html );
}

//
// Take single snapshot and add to the list
//
function take_snapshot() {
  // take snapshot and get image data
  Webcam.snap( show_snapshot );
}

//
// Clear snapshots panel
//
function clear_snapshots() {
  $('#snapshots').html( '' );
}

//
// Add resolution buttons based on 'resolutions' array
//
function insert_resolution_form() {
  var html = '<form action="none" style="display: inline;">';

  resolutions.forEach((r) => {
    var button = ``
      + `<button type="button"`
      + ` class="resolution-button btn btn-info btn-xs"`
      + ` onclick="webcam_start(${r.width}, ${r.height});"`
      + ` id="${r.btnId}"`
      + `>${r.width}x${r.height}</button>`
      + ``;
    html += button;
  });

  html += '</form>';

  $('#resolution-buttons').html( html );
}

//
// Startup code
//
window.onload = function() {
  insert_resolution_form();
  webcam_start(640, 480);

  // Attach a listener for webcam initialization
  Webcam.on( 'live', function() {
    $('#camera-preview').css('margin-top', '1em');
    $('#camera-preview').css('margin-bottom', '1em');
    $('#camera-preview').css('margin-left', 'auto');
    $('#camera-preview').css('margin-right', 'auto');

    // Re-check if Fullscreen mode is available
    $('#btn-fullscreen').remove();
    var video = document.getElementsByTagName('video')[0];
    if (video && video.requestFullscreen) {
    // if ($('#camera-preview').width() >= 800) {
      $('#resolution-buttons').append(''
        + '<button type="button"'
        + ' id="btn-fullscreen"'
        + ' class="btn btn-info btn-xs resolution-button"'
        + ' onclick="start_fullscreen();"'
        + '><span class="glyphicon glyphicon-fullscreen"></span></button>'
        + '');
    } else {
      $('#resolution-buttons').append(''
        + '<button type="button"'
        + ' id="btn-fullscreen"'
        + ' class="btn btn-primary btn-xs disabled resolution-button"'
        + '><span class="glyphicon glyphicon-fullscreen"></span></button>'
        + '');
    }
  } );
}
