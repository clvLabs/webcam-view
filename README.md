#Webcam View

A webcam viewer in a web page (built as an exercise).

![screen capture](images/screencap.png)


Based on:

* [Webpack Boilerplate](https://github.com/geniuscarrier/webpack-boilerplate)
* [WebcamJS](https://github.com/jhuckaby/webcamjs)
* [Bootstrap](http://getbootstrap.com/)
* [Slate](https://bootswatch.com/slate/) (Bootstrap theme)

###Install dependencies

Make sure you have [npm](https://www.npmjs.com/) installed and then

```
npm install
```

###Developing locally

1. Run

	```
	npm run dev
	```

2. In your browser, navigate to: [http://localhost:8080/](http://localhost:8080/)

###Publish

1. In `webpack.config.js` file, replace `www.example.com` with the real domain name.

2. Run

	```
	npm run build
	```

3. Copy the following folders/files to web server

	/build,
	/image,
	index.html
